﻿using System;

namespace assignment_1_hello_you
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello you!");
            Console.WriteLine("My name is Mari! What is your name?");
            string name = Console.ReadLine();

            Console.WriteLine("Hello, " + name + ". Your name is " + name.Length + " characters long and starts with a(n) " + name.Substring(0,1) + ".");
            Console.WriteLine($"You can also write lines like this, {name}. And also print the length: {name.Length} using this method:");
            Console.WriteLine("You can also write lines like this, {name}. And also print the length: {name.Length}");
        }
    }
}
